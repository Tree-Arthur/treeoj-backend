### 简介

「力扣编辑器」本质上是一个特别的 Markdown 编辑器，之所以特别是因为她不仅支持了传统的 Markdown 语法，同时还支持力扣独有的特殊格式和功能。

---

### 支持语法

#### 标题

# heading 1
## heading 2
### heading 3
#### heading 4
##### heading 5
###### heading 6

---


#### 引用

> 在 `>` 后插入您所希望应用的内容。

---

#### 列表

> 无序列表

- 文本一
- 文本二
- 文本三

> 有序列表

1. 文本一
2. 文本二
3. 文本三

---

#### 文字变形

_斜体_
*斜体*
 
__粗体__
**粗体**

---

#### Latex

行内 Latex 支持：$O(n^2)$。

$$m \leq n,\  i < m \implies j = \frac{m+n+1}{2} - i > \frac{m+n+1}{2} - m \geq \frac{2m+1}{2} - m \geq 0$$

> 需要独行 Latex，请使用 `$$` 包围。

---

#### 代码

使用一段 `行内代码`。

> 代码片段

```java
public class Main {
    public static void main(String[] args) {
        System.out.print("你好力扣");
    }
}
```

> 组合代码模块

```java [solution1-Java 答案]
public class Main {
    public static void main(String[] args) {
        System.out.print("你好力扣");
    }
}
```
```python [solution1-Python 答案]
print "你好力扣"
```

---

#### 表格

[//]: # (名称 | 缩写)

[//]: # (--- | ---)

[//]: # (JavaScript | js)

[//]: # (Python | py)

[//]: # (C++ | cpp)

> 使用冒号定义对齐方式

| 题号 | 标题 | 难易度 |
| :--- | ---:| :---: |
| 1 | 两数之和 | 简单 |
| 15 | 三数之和 | 中等 |
| 262 | 行程和用户 | 困难 |

---

#### 链接

[这是一个超级链接](https://leetcode.cn)

---

#### 图片

![力扣](https://pic.leetcode.cn/056dd17dd0506591d04525f015765ab749e9f9f19dcaf6e0dcae5d77057d2392-15@3x.png){:width=100}

> 使用传统 `![图片名称](图片地址)` Markdown 语法，或将图片直接拖拽到编辑器中进行上传。
>
> 可以在图片尾部添加 `{:width=宽度像素}` 限制图片宽度，或者 `{:height=高度像素}` 限制图片高度。

---

#### 视频

![力扣视频](562986b1-46d8-48ef-bac6-82685bcedcd8)


> 使用工具栏中「视频上传」工具进行上传，支持 m4v、mp4、mov、flv 等多种主流视频格式。
> 视频内容需要等待人工审核完成后才能执行播放，审核期间不影响内容发布。

---

### 结尾

为了让您的内容可以给大家带来最佳的阅读体验，我们推荐您遵循「[中文文案排版指北](https://github.com/sparanoid/chinese-copywriting-guidelines/blob/master/README.zh-CN.md)」。

更多 Markdown 语法，请查看 [官方文档](https://www.markdownguide.org/basic-syntax/)。

最后，感谢您抽空阅读本「力扣编辑器」使用说明，祝您创作愉快！

---

---
# frontmatter: https://jekyllrb.com/docs/front-matter/
layout: post
title: Blogging Like a Hacker
---

## Markdown Basic Syntax

I just love **bold text**. Italicized text is the _cat's meow_. At the command prompt, type `nano`.

My favorite markdown editor is [ByteMD](https://github.com/bytedance/bytemd).

1. First item
2. Second item
3. Third item

> Dorothy followed her through many of the beautiful rooms in her castle.

```js
import gfm from '@bytemd/plugin-gfm'
import { Editor, Viewer } from 'bytemd'

const plugins = [
  gfm(),
  // Add more plugins here
]

const editor = new Editor({
  target: document.body, // DOM to render
  props: {
    value: '',
    plugins,
  },
})

editor.on('change', (e) => {
  editor.$set({ value: e.detail.value })
})
```

## GFM Extended Syntax

Automatic URL Linking: https://github.com/bytedance/bytemd

~~The world is flat.~~ We now know that the world is round.

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media

| Syntax    | Description |
| --------- | ----------- |
| Header    | Title       |
| Paragraph | Text        |

## Footnotes

Here's a simple footnote,[^1] and here's a longer one.[^bignote]

[^1]: This is the first footnote.
[^bignote]: Here's one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.

## Gemoji

Thumbs up: :+1:, thumbs down: :-1:.

Families: :family_man_man_boy_boy:

Long flags: :wales:, :scotland:, :england:.

## Math Equation

Inline math equation: $a+b$

$$
\displaystyle \left( \sum_{k=1}^n a_k b_k \right)^2 \leq \left( \sum_{k=1}^n a_k^2 \right) \left( \sum_{k=1}^n b_k^2 \right)
$$

## Mermaid Diagrams

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```