package com.tree.treeoj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tree.treeoj.common.BaseResponse;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.common.ResultUtils;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.exception.ThrowUtils;
import com.tree.treeoj.model.dto.postthumb.PostThumbAddRequest;
import com.tree.treeoj.model.dto.question.QuestionQueryRequest;
import com.tree.treeoj.model.dto.questionsubmit.QuestionSubmitAddRequest;
import com.tree.treeoj.model.dto.questionsubmit.QuestionSubmitQueryRequest;
import com.tree.treeoj.model.entity.Question;
import com.tree.treeoj.model.entity.QuestionSubmit;
import com.tree.treeoj.model.entity.User;
import com.tree.treeoj.model.vo.QuestionSubmitVO;
import com.tree.treeoj.model.vo.QuestionVO;
import com.tree.treeoj.service.QuestionSubmitService;
import com.tree.treeoj.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 题目提交接口
 *
 */
@RestController
@RequestMapping("/question_submit")
@Slf4j
@Deprecated
public class QuestionSubmitController {

    @Resource
    private QuestionSubmitService questionSubmitService;

    @Resource
    private UserService userService;

    /**
     * 题目提交数
     *
     * @param questionSubmitAddRequest
     * @param request
     * @return resultNum 提交记录ID
     */
    @PostMapping("/")
    public BaseResponse<Long> doQuestionSubmit(@RequestBody QuestionSubmitAddRequest questionSubmitAddRequest,
                                         HttpServletRequest request) {
        if (questionSubmitAddRequest == null || questionSubmitAddRequest.getQuestionId() <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        // 登录才能点赞
        final User loginUser = userService.getLoginUser(request);
        Long questionSubmitId = questionSubmitService.doQuestionSubmit(questionSubmitAddRequest, loginUser);
        return ResultUtils.success(questionSubmitId);
    }

    /**
     * 分页获取题目提交列表（除了管理员外，其他普通用户只能看到非答案、提交的代码等公开信息）
     *
     * @param questionSubmitQueryRequest
     * @param request
     * @return
     */
    @PostMapping("/list/page/vo")
    @ApiOperation(value = "分页获取题目提交列表")
    public BaseResponse<Page<QuestionSubmitVO>> listMyQuestionVOByPage(@RequestBody QuestionSubmitQueryRequest questionSubmitQueryRequest,
                                                                       HttpServletRequest request) {
        if (questionSubmitQueryRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        questionSubmitQueryRequest.setUserId(loginUser.getId());
        long current = questionSubmitQueryRequest.getCurrent();
        long size = questionSubmitQueryRequest.getPageSize();
        // 限制爬虫
        ThrowUtils.throwIf(size > 20, ErrorCode.PARAMS_ERROR);
        // 从数据库中查询到原始的题目提交信息
        Page<QuestionSubmit> questionSubmitPage = questionSubmitService.page(new Page<>(current, size),
                questionSubmitService.getQueryWrapper(questionSubmitQueryRequest));
        // 返回脱敏信息
        return ResultUtils.success(questionSubmitService.getQuestionSubmitVOPage(questionSubmitPage, loginUser));
    }

}
