package com.tree.treeoj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.tree.treeoj.annotation.AuthCheck;
import com.tree.treeoj.common.BaseResponse;
import com.tree.treeoj.common.DeleteRequest;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.common.ResultUtils;
import com.tree.treeoj.config.WxOpenConfig;
import com.tree.treeoj.model.entity.User;
import com.tree.treeoj.model.entity.UserInfo;
import com.tree.treeoj.model.enums.UserRoleEnum;
import com.tree.treeoj.model.vo.UserInfoVO;
import com.tree.treeoj.service.UserInfoService;
import com.tree.treeoj.service.UserService;
import com.tree.treeoj.constant.UserConstant;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.exception.ThrowUtils;
import com.tree.treeoj.model.dto.user.UserAddRequest;
import com.tree.treeoj.model.dto.user.UserLoginRequest;
import com.tree.treeoj.model.dto.user.UserQueryRequest;
import com.tree.treeoj.model.dto.user.UserRegisterRequest;
import com.tree.treeoj.model.dto.user.UserUpdateMyRequest;
import com.tree.treeoj.model.dto.user.UserUpdateRequest;
import com.tree.treeoj.model.vo.LoginUserVO;
import com.tree.treeoj.model.vo.UserVO;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户接口
 *
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Resource
    private UserService userService;
    @Resource
    private UserInfoService userInfoService;

    @Resource
    private WxOpenConfig wxOpenConfig;

    // region 登录相关

    /**
     * 用户注册
     *
     * @param userRegisterRequest
     * @return
     */
    @PostMapping("/register")
    @ApiOperation(value = "用户注册")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest userRegisterRequest) {
        if (userRegisterRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            return null;
        }
        long result = userService.userRegister(userAccount, userPassword, checkPassword);
        return ResultUtils.success(result);
    }

    /**
     * 用户登录
     *
     * @param userLoginRequest
     * @param request
     * @return
     */
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public BaseResponse<LoginUserVO> userLogin(@RequestBody UserLoginRequest userLoginRequest, HttpServletRequest request) {
        if (userLoginRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        LoginUserVO loginUserVO = userService.userLogin(userAccount, userPassword, request);
        return ResultUtils.success(loginUserVO);
    }

    /**
     * 用户注销
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    @ApiOperation(value = "用户注销")
    public BaseResponse<Boolean> userLogout(HttpServletRequest request) {
        if (request == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean result = userService.userLogout(request);
        return ResultUtils.success(result);
    }

    /**
     * 获取当前登录用户
     *
     * @param request
     * @return
     */
    @GetMapping("/get/login")
    @ApiOperation(value = "获取当前登录用户")
    public BaseResponse<LoginUserVO> getLoginUser(HttpServletRequest request) {
        User user = userService.getLoginUser(request);
        return ResultUtils.success(userService.getLoginUserVO(user));
    }

    // endregion

    // region 增删改查

    /**
     * 创建用户
     *
     * @param userAddRequest
     * @param request
     * @return
     */
    @PostMapping("/add")
    @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
    @ApiOperation(value = "创建用户")
    public BaseResponse<Long> addUser(@RequestBody UserAddRequest userAddRequest, HttpServletRequest request) {
        if (userAddRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = new User();
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userAddRequest, user);
        Long userId = user.getId();
        userInfo.setUserId(userId);
        boolean userResult = userService.save(user);
        boolean userInfoResult = userInfoService.save(userInfo);
        ThrowUtils.throwIf(!userResult, ErrorCode.OPERATION_ERROR);
        ThrowUtils.throwIf(!userInfoResult, ErrorCode.OPERATION_ERROR);
        return ResultUtils.success(userId);
    }
    /**
     * 封禁用户
     *
     * @param userId
     * @param request
     * @return
     */
    @GetMapping("/ban")
    @ApiOperation(value = "封禁用户")
    @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
    public BaseResponse<Boolean> banUser(@RequestParam long userId, HttpServletRequest request) {
        if (userId <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = userService.getById(userId);
        if (!"ban".equals(user.getUserRole())){
            user.setUserRole(UserRoleEnum.BAN.getValue());//封号
            boolean ban = userService.updateById(user);
            System.out.println("用户已封禁");
            return ResultUtils.success(ban);
        }
        user.setUserRole(UserRoleEnum.USER.getValue());//解封
        boolean unban = userService.updateById(user);
        System.out.println("用户已解封禁");
        return ResultUtils.success(unban);
    }

    /**
     * 删除用户
     *
     * @param deleteRequest
     * @param request
     * @return
     */
    @PostMapping("/delete")
    @ApiOperation(value = "删除用户")
    @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
    public BaseResponse<Boolean> deleteUser(@RequestBody DeleteRequest deleteRequest, HttpServletRequest request) {
        if (deleteRequest == null || deleteRequest.getId() <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean delete = userService.removeById(deleteRequest.getId());
        return ResultUtils.success(delete);
    }

    /**
     * 更新用户
     *
     * @param userUpdateRequest
     * @param request
     * @return
     */
    @PostMapping("/update")
    @ApiOperation(value = "更新用户")
    @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
    public BaseResponse<Boolean> updateUser(@RequestBody UserUpdateRequest userUpdateRequest,
            HttpServletRequest request) {
        if (userUpdateRequest == null || userUpdateRequest.getId() == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = new User();
        BeanUtils.copyProperties(userUpdateRequest, user);
        boolean userResult = userService.updateById(user);
        ThrowUtils.throwIf(!userResult, ErrorCode.OPERATION_ERROR);

        UserInfoVO userInfoVo = userUpdateRequest.getUserInfoVo();
        UserInfo userInfo = UserInfoVO.voToObj(userInfoVo);
        boolean userInfoResult = userInfoService.updateById(userInfo);
        ThrowUtils.throwIf(!userInfoResult, ErrorCode.OPERATION_ERROR);
        return ResultUtils.success(true);
    }

    /**
     * 根据 id 获取用户（仅管理员）
     *
     * @param id
     * @param request
     * @return
     */
    @GetMapping("/get")
    @ApiOperation(value = "根据 id 获取用户（仅管理员）")
    @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
    public BaseResponse<User> getUserById(long id, HttpServletRequest request) {
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = userService.getById(id);
        user = userService.getUser(user);
        ThrowUtils.throwIf(user == null, ErrorCode.NOT_FOUND_ERROR);
        return ResultUtils.success(user);
    }

    /**
     * 根据 id 获取包装类
     *
     * @param id
     * @param request
     * @return
     */
    @GetMapping("/get/vo")
    @ApiOperation(value = "根据 id 获取包装类")
    public BaseResponse<UserVO> getUserVOById(long id, HttpServletRequest request) {
        BaseResponse<User> response = getUserById(id, request);
        User user = response.getData();
        return ResultUtils.success(userService.getUserVO(user));
    }

    /**
     * 分页获取用户列表（仅管理员）
     *
     * @param userQueryRequest
     * @param request
     * @return
     */
    @PostMapping("/list/page")
    @ApiOperation(value = "分页获取用户列表（仅管理员）")
    @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
    public BaseResponse<Page<User>> listUserByPage(@RequestBody UserQueryRequest userQueryRequest,
            HttpServletRequest request) {
        long current = userQueryRequest.getCurrent();
        long size = userQueryRequest.getPageSize();
        Page<User> userPage = userService.page(new Page<>(current, size),
                userService.getQueryWrapper(userQueryRequest));
        List<User> user = userService.getUser(userPage.getRecords());

//        UserInfoQueryRequest userInfoQueryRequest = new UserInfoQueryRequest();
//        BeanUtils.copyProperties(userQueryRequest,userInfoQueryRequest);
//        Page<UserInfo> userInfoPage = userInfoService.page(new Page<>(current, size), userInfoService.getQueryWrapper(userInfoQueryRequest));
//        List<UserInfo> userInfo = userInfoService.getUserInfo(userInfoPage.getRecords());
//        //stream流映射
//        user.stream().map().collect(Collectors.toList())

        userPage.setRecords(user);
        return ResultUtils.success(userPage);
    }

    /**
     * 分页获取用户封装列表
     *
     * @param userQueryRequest
     * @param request
     * @return
     */
    @PostMapping("/list/page/vo")
    @ApiOperation(value = "分页获取用户封装列表")
    public BaseResponse<Page<UserVO>> listUserVOByPage(@RequestBody UserQueryRequest userQueryRequest,
            HttpServletRequest request) {
        if (userQueryRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        long current = userQueryRequest.getCurrent();
        long size = userQueryRequest.getPageSize();
        // 限制爬虫
        ThrowUtils.throwIf(size > 20, ErrorCode.PARAMS_ERROR);
        Page<User> userPage = userService.page(new Page<>(current, size),
                userService.getQueryWrapper(userQueryRequest));
        Page<UserVO> userVOPage = new Page<>(current, size, userPage.getTotal());
        List<UserVO> userVO = userService.getUserVO(userPage.getRecords());
        userVOPage.setRecords(userVO);
        return ResultUtils.success(userVOPage);
    }

    // endregion
    private final static Gson GSON = new Gson();

    /**
     * 更新个人信息
     *
     * @param userUpdateMyRequest
     * @param request
     * @return
     */
    @PostMapping("/update/my")
    @ApiOperation(value = "更新个人信息")
    public BaseResponse<Boolean> updateMyUser(@RequestBody UserUpdateMyRequest userUpdateMyRequest,
            HttpServletRequest request) {
        if (userUpdateMyRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        Long userId = loginUser.getId();
        //更新用户信息
        User user = new User();
        BeanUtils.copyProperties(userUpdateMyRequest, user);
        user.setId(userId);
        boolean userResult = userService.updateById(user);
        ThrowUtils.throwIf(!userResult, ErrorCode.OPERATION_ERROR);
        //更新用户其他信息
        UserInfo userInfo = new UserInfo();
        UserInfoVO userInfoVo = userUpdateMyRequest.getUserInfoVo();
        BeanUtils.copyProperties(userInfoVo, userInfo);
        List<String> tags = userInfoVo.getTagList();
        if (tags != null) {
            userInfo.setTags(GSON.toJson(tags));
        }
        boolean userInfoResult = userInfoService.updateById(userInfo);
        ThrowUtils.throwIf(!userInfoResult, ErrorCode.OPERATION_ERROR);
        return ResultUtils.success(true);
    }
}
