package com.tree.treeoj.constant;

/**
 * 用户常量
 */
public interface UserConstant {

    /**
     * 用户登录态键
     */
    String USER_LOGIN_STATE = "user_login";
    String USER_LOGOUT_STATE = "user_logout";

    //  region 权限

    /**
     * 默认角色
     */
    String DEFAULT_ROLE = "user";

    /**
     * 管理员角色
     */
    String ADMIN_ROLE = "admin";
    /**
     * 超级管理员角色
     */
    String ADMINISTRATOR_ROLE = "administrator";
    /**
     * 被封号
     */
    String BAN_ROLE = "ban";

    // endregion
}
