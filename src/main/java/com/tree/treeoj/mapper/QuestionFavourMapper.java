package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.QuestionFavour;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 11818
* @description 针对表【question_favour(题目收藏)】的数据库操作Mapper
* @createDate 2024-02-25 18:43:42
* @Entity com.tree.treeoj.model.entity.QuestionFavour
*/
public interface QuestionFavourMapper extends BaseMapper<QuestionFavour> {

}




