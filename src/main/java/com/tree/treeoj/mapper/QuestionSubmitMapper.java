package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.QuestionSubmit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Tree Arthur
* @description 针对表【question_submit(题目提交)】的数据库操作Mapper
* @createDate 2023-12-19 02:18:22
* @Entity com.tree.treeoj.model.entity.QuestionSubmit
*/
public interface QuestionSubmitMapper extends BaseMapper<QuestionSubmit> {

}




