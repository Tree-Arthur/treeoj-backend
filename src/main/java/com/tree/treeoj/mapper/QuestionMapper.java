package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Tree Arthur
* @description 针对表【question(题目)】的数据库操作Mapper
* @createDate 2023-12-19 02:17:26
* @Entity com.tree.treeoj.model.entity.Question
*/
public interface QuestionMapper extends BaseMapper<Question> {

}




