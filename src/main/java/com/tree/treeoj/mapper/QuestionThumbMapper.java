package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.QuestionThumb;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 11818
* @description 针对表【question_thumb(题目点赞)】的数据库操作Mapper
* @createDate 2024-02-25 18:43:50
* @Entity com.tree.treeoj.model.entity.QuestionThumb
*/
public interface QuestionThumbMapper extends BaseMapper<QuestionThumb> {

}




