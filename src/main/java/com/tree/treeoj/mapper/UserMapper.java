package com.tree.treeoj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tree.treeoj.model.entity.User;

/**
 * 用户数据库操作
 *
 */
public interface UserMapper extends BaseMapper<User> {

}




