package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.QuestionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 11818
* @description 针对表【question_relation(题库关联题目)】的数据库操作Mapper
* @createDate 2024-03-14 11:37:03
* @Entity com.tree.treeoj.model.entity.QuestionRelation
*/
public interface QuestionRelationMapper extends BaseMapper<QuestionRelation> {

}




