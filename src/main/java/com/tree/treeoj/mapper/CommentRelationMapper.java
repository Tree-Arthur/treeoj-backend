package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.CommentRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 11818
* @description 针对表【comment_relation(评论关联)】的数据库操作Mapper
* @createDate 2024-02-26 12:22:54
* @Entity com.tree.treeoj.model.entity.CommentRelationAddRequest
*/
public interface CommentRelationMapper extends BaseMapper<CommentRelation> {

}




