package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.PostComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 11818
* @description 针对表【post_comment(帖子评论)】的数据库操作Mapper
* @createDate 2024-02-26 12:22:48
* @Entity com.tree.treeoj.model.entity.PostComment
*/
public interface PostCommentMapper extends BaseMapper<PostComment> {

}




