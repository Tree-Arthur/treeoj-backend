package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 11818
* @description 针对表【user_info(用户信息)】的数据库操作Mapper
* @createDate 2024-02-23 09:56:44
* @Entity com.tree.treeoj.model.entity.UserInfo
*/
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}




