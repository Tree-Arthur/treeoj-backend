package com.tree.treeoj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tree.treeoj.model.entity.QuestionBank;

/**
* @author 11818
* @description 针对表【question_bank(题库)】的数据库操作Mapper
* @createDate 2024-03-14 11:34:45
* @Entity com.tree.treeoj.model.entity.QuestionBank
*/
public interface QuestionBankMapper extends BaseMapper<QuestionBank> {

}




