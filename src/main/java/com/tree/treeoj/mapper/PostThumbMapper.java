package com.tree.treeoj.mapper;

import com.tree.treeoj.model.entity.PostThumb;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 帖子点赞数据库操作
 *
 */
public interface PostThumbMapper extends BaseMapper<PostThumb> {

}




