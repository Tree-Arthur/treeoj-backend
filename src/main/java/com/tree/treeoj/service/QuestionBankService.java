package com.tree.treeoj.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tree.treeoj.model.dto.questionbank.QuestionBankQueryRequest;
import com.tree.treeoj.model.entity.QuestionBank;
import com.tree.treeoj.model.vo.QuestionBankVO;

import javax.servlet.http.HttpServletRequest;

/**
* @author 11818
* @description 针对表【question_bank(题库)】的数据库操作Service
* @createDate 2024-03-14 11:34:45
*/
public interface QuestionBankService extends IService<QuestionBank> {

    void validQuestionBank(QuestionBank questionBank, boolean b);

    QuestionBankVO getQuestionBankVo(QuestionBank questionBank, HttpServletRequest request);

    Page<QuestionBankVO> getQuestionBankVoPage(Page<QuestionBank> questionBankPage, HttpServletRequest request);

    QueryWrapper<QuestionBank> getQueryWrapper(QuestionBankQueryRequest questionBankQueryRequest);
}
