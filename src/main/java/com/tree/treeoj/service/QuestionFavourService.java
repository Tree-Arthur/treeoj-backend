package com.tree.treeoj.service;

import com.tree.treeoj.model.entity.QuestionFavour;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 11818
* @description 针对表【question_favour(题目收藏)】的数据库操作Service
* @createDate 2024-02-25 18:43:42
*/
public interface QuestionFavourService extends IService<QuestionFavour> {

}
