package com.tree.treeoj.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tree.treeoj.model.dto.comment.PostCommentQueryRequest;
import com.tree.treeoj.model.dto.post.PostQueryRequest;
import com.tree.treeoj.model.entity.Post;
import com.tree.treeoj.model.entity.PostComment;
import com.tree.treeoj.model.vo.PostCommentVO;
import com.tree.treeoj.model.vo.PostVO;

import javax.servlet.http.HttpServletRequest;

/**
* @author 11818
* @description 针对表【post_comment(帖子评论)】的数据库操作Service
* @createDate 2024-02-26 12:22:48
*/
public interface PostCommentService extends IService<PostComment> {

    /**
     * 校验
     *
     * @param postComment
     * @param add
     */
    void validPostComment(PostComment postComment, boolean add);

    /**
     * 获取帖子评论封装
     *
     * @param postComment
     * @param request
     * @return
     */
    PostCommentVO getPostCommentVO(PostComment postComment , HttpServletRequest request);
    /**
     * 分页获取帖子评论封装
     *
     * @param postCommentPage
     * @param request
     * @return
     */
    Page<PostCommentVO> getPostCommentVOPage(Page<PostComment> postCommentPage, HttpServletRequest request);
    /**
     * 获取查询条件
     *
     * @param postCommentQueryRequest
     * @return
     */
    QueryWrapper<PostComment> getQueryWrapper(PostCommentQueryRequest postCommentQueryRequest);
}
