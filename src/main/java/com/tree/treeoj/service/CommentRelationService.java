package com.tree.treeoj.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tree.treeoj.model.dto.comment.CommentQueryRequest;
import com.tree.treeoj.model.dto.comment.PostCommentQueryRequest;
import com.tree.treeoj.model.entity.CommentRelation;
import com.tree.treeoj.model.entity.PostComment;
import com.tree.treeoj.model.vo.CommentVO;
import com.tree.treeoj.model.vo.PostCommentVO;

import javax.servlet.http.HttpServletRequest;

/**
* @author 11818
* @description 针对表【comment_relation(评论关联)】的数据库操作Service
* @createDate 2024-02-26 12:22:55
*/
public interface CommentRelationService extends IService<CommentRelation> {

    void validComment(CommentRelation commentRelation, boolean add);
    /**
     * 获取帖子评论封装
     *
     * @param comment
     * @param request
     * @return
     */
    CommentVO getCommentVO(CommentRelation comment , HttpServletRequest request);
    /**
     * 分页获取帖子评论封装
     *
     * @param commentPage
     * @param request
     * @return
     */
    Page<CommentVO> getCommentVOPage(Page<CommentRelation> commentPage, HttpServletRequest request);
    /**
     * 获取查询条件
     *
     * @param commentQueryRequest
     * @return
     */
    QueryWrapper<CommentRelation> getQueryWrapper(CommentQueryRequest commentQueryRequest);
}
