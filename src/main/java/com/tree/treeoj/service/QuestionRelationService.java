package com.tree.treeoj.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tree.treeoj.model.dto.question.QuestionRelationQueryRequest;
import com.tree.treeoj.model.entity.QuestionRelation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tree.treeoj.model.entity.User;
import com.tree.treeoj.model.vo.QuestionRelationVO;

/**
* @author 11818
* @description 针对表【question_relation(题库关联题目)】的数据库操作Service
* @createDate 2024-03-14 11:37:03
*/
public interface QuestionRelationService extends IService<QuestionRelation> {

    /**
     * 校验
     * @param questionRelation
     * @param add
     */
    void validQuestionRelation(QuestionRelation questionRelation, boolean add);

    QueryWrapper<QuestionRelation> getQueryWrapper(QuestionRelationQueryRequest questionRelationQueryRequest);

    /**
     * 分页获取题目封装
     * @param questionRelationPage
     * @param request
     * @return
     */
    Page<QuestionRelationVO> getQuestionRelationVOPage(Page<QuestionRelation> questionRelationPage, User request);
}
