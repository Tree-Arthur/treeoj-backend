package com.tree.treeoj.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tree.treeoj.model.dto.post.PostQueryRequest;
import com.tree.treeoj.model.dto.question.QuestionQueryRequest;
import com.tree.treeoj.model.entity.Post;
import com.tree.treeoj.model.entity.Question;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tree.treeoj.model.entity.User;
import com.tree.treeoj.model.vo.PostVO;
import com.tree.treeoj.model.vo.QuestionVO;

import javax.servlet.http.HttpServletRequest;

/**
* @author Tree Arthur
* @description 针对表【question(题目)】的数据库操作Service
* @createDate 2023-12-19 02:17:26
*/
public interface QuestionService extends IService<Question> {

    /**
     * 校验题目是否合法
     *
     * @param question
     * @param add
     */
    void validQuestion(Question question, boolean add);
    /**
     * 获取查询条件
     *
     * @param questionQueryRequest
     * @return
     */
    QueryWrapper<Question> getQueryWrapper(QuestionQueryRequest questionQueryRequest);

    /**
     * 从 ES 查询
     *
     * @param questionQueryRequest
     * @return
     */
//    Page<Question> searchFromEs(QuestionQueryRequest questionQueryRequest);

    /**
     * 获取题目封装
     *
     * @param question
     * @param loginUser
     * @return
     */
    QuestionVO getQuestionVO(Question question, User loginUser);

    /**
     * 分页获取题目封装
     *
     * @param questionPage
     * @param request
     * @return
     */
    Page<QuestionVO> getQuestionVOPage(Page<Question> questionPage, HttpServletRequest request);
}
