package com.tree.treeoj.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tree.treeoj.model.dto.userinfo.UserInfoQueryRequest;
import com.tree.treeoj.model.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tree.treeoj.model.vo.UserInfoVO;

import java.util.List;

/**
 * @author tree
 * @description 针对表【user_info(用户信息)】的数据库操作Service
 * @createDate 2024-02-23 09:56:44
 */
public interface UserInfoService extends IService<UserInfo> {

    /**
     * 获取用户其他信息包装类
     *
     * @param userInfo
     * @return
     */
    UserInfoVO getUserInfoVo(UserInfo userInfo);


    /**
     * 获取查询包装类（用户根据哪些字段查询，根据前端传来的请求对象得到MyBatis支持的QueryWrapper）
     *
     * @param userInfoQueryRequest
     * @return
     */
    QueryWrapper<UserInfo> getQueryWrapper(UserInfoQueryRequest userInfoQueryRequest);

    /**
     * 根据用户ID获取用户其他信息
     * @param userId
     * @return
     */
    UserInfo getUserInfo(Long userId);

    UserInfo getUserInfo(UserInfo userInfo);

    List<UserInfo> getUserInfo(List<UserInfo> userInfoList);
}
