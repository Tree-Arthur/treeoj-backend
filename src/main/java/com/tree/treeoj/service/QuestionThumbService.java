package com.tree.treeoj.service;

import com.tree.treeoj.model.entity.QuestionThumb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 11818
* @description 针对表【question_thumb(题目点赞)】的数据库操作Service
* @createDate 2024-02-25 18:43:50
*/
public interface QuestionThumbService extends IService<QuestionThumb> {

}
