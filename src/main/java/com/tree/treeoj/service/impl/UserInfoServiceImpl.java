package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.constant.CommonConstant;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.mapper.UserInfoMapper;
import com.tree.treeoj.model.dto.userinfo.UserInfoQueryRequest;
import com.tree.treeoj.model.entity.UserInfo;
import com.tree.treeoj.model.vo.UserInfoVO;
import com.tree.treeoj.service.UserInfoService;
import com.tree.treeoj.utils.SqlUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author tree
* @description 针对表【user_info(用户信息)】的数据库操作Service实现
* @createDate 2024-02-23 09:56:44
*/
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo>
    implements UserInfoService{

    @Override
    public UserInfoVO getUserInfoVo(UserInfo userInfo) {
        UserInfoVO userInfoVo = UserInfoVO.objToVo(userInfo);

        return userInfoVo;
    }

    @Override
    public QueryWrapper<UserInfo> getQueryWrapper(UserInfoQueryRequest userInfoQueryRequest) {
        if (userInfoQueryRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请求参数为空");
        }
        Long id = userInfoQueryRequest.getId();
        Long userId = userInfoQueryRequest.getUserId();
        Integer gender = userInfoQueryRequest.getGender();
        String phone = userInfoQueryRequest.getPhone();
        String email = userInfoQueryRequest.getEmail();
        Integer userStatus = userInfoQueryRequest.getUserStatus();
        String tags = userInfoQueryRequest.getTags();
        String sortField = userInfoQueryRequest.getSortField();
        String sortOrder = userInfoQueryRequest.getSortOrder();

        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(id != null, "id", id);
        queryWrapper.eq(userId != null, "userId", userId);
        queryWrapper.eq(gender != null, "gender", gender);
        queryWrapper.eq(userStatus != null, "userStatus", userStatus);
        //模糊
        queryWrapper.like(StringUtils.isNotBlank(phone), "phone", phone);
        queryWrapper.like(StringUtils.isNotBlank(email), "email", email);
        queryWrapper.like(StringUtils.isNotBlank(tags), "tags", tags);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);
        return queryWrapper;
    }

    @Override
    public UserInfo getUserInfo(UserInfo userInfo) {
        if (userInfo == null) {
            return null;
        }
        return userInfo;
    }

    /**
     * 获取用户其他信息
     * @param userId
     * @return
     */
    @Override
    public UserInfo getUserInfo(Long userId) {
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("userId", userId);
        UserInfo userInfo = this.getOne(userInfoQueryWrapper);
        return userInfo;
    }

    @Override
    public List<UserInfo> getUserInfo(List<UserInfo> userInfoList) {
        if (CollectionUtils.isEmpty(userInfoList)) {
            return new ArrayList<>();
        }
        return userInfoList.stream().map(this::getUserInfo).collect(Collectors.toList());
    }
}




