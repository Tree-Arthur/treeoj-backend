package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.constant.CommonConstant;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.exception.ThrowUtils;
import com.tree.treeoj.model.dto.comment.CommentQueryRequest;
import com.tree.treeoj.model.dto.comment.PostCommentQueryRequest;
import com.tree.treeoj.model.entity.CommentRelation;
import com.tree.treeoj.mapper.CommentRelationMapper;
import com.tree.treeoj.model.entity.PostComment;
import com.tree.treeoj.model.entity.User;
import com.tree.treeoj.model.vo.CommentVO;
import com.tree.treeoj.model.vo.PostCommentVO;
import com.tree.treeoj.model.vo.UserVO;
import com.tree.treeoj.service.CommentRelationService;
import com.tree.treeoj.service.PostCommentService;
import com.tree.treeoj.service.UserService;
import com.tree.treeoj.utils.SqlUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author 11818
* @description 针对表【comment_relation(评论关联)】的数据库操作Service实现
* @createDate 2024-02-26 12:22:54
*/
@Service
public class CommentRelationServiceImpl extends ServiceImpl<CommentRelationMapper, CommentRelation>
    implements CommentRelationService {
    @Resource
    private UserService userService;
    @Resource
    private PostCommentService postCommentService;

    @Override
    public void validComment(CommentRelation commentRelation, boolean add) {
        if (commentRelation == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long postId = commentRelation.getPostId();
        String sonId = commentRelation.getSonId();
        String targetId = commentRelation.getTargetId();
        String content = commentRelation.getContent();
        Long userId = commentRelation.getUserId();
        // 创建时，参数不能为空
        if (add) {
            ThrowUtils.throwIf(StringUtils.isAnyBlank(content), ErrorCode.PARAMS_ERROR);
            ThrowUtils.throwIf(StringUtils.isAnyBlank(sonId), ErrorCode.PARAMS_ERROR);
            ThrowUtils.throwIf(StringUtils.isAnyBlank(targetId), ErrorCode.PARAMS_ERROR);
            ThrowUtils.throwIf(postId == null, ErrorCode.PARAMS_ERROR);
        }
        // 有参数则校验
        if (StringUtils.isNotBlank(content) && content.length() > 80) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "评论内容过长");
        }
    }

    @Override
    public CommentVO getCommentVO(CommentRelation comment, HttpServletRequest request) {
        CommentVO commentVO = CommentVO.objToVo(comment);

        return commentVO;
    }

    @Override
    public Page<CommentVO> getCommentVOPage(Page<CommentRelation> commentPage, HttpServletRequest request) {
        List<CommentRelation> commentRelationList = commentPage.getRecords();
        Page<CommentVO> commentVOPage = new Page<>(commentPage.getCurrent(), commentPage.getSize(), commentPage.getTotal());
        if (CollectionUtils.isEmpty(commentRelationList)) {
            return commentVOPage;
        }
        // 填充信息
        List<CommentVO> commentVOList = commentRelationList.stream().map(comment -> {
            CommentVO commentVO = CommentVO.objToVo(comment);
            String userName = userService.getById(comment.getUserId()).getUserName();
            Long targetUserId = null;
            QueryWrapper<PostComment> postCommentQueryWrapper = new QueryWrapper<>();
            postCommentQueryWrapper.eq("fatherId",comment.getTargetId());
            PostComment postComment = postCommentService.getOne(postCommentQueryWrapper);

            QueryWrapper<CommentRelation> commentRelationQueryWrapper = new QueryWrapper<>();
            commentRelationQueryWrapper.eq("sonId",comment.getTargetId());
            CommentRelation commentRelation = this.getOne(commentRelationQueryWrapper);
            if (postComment != null){
                targetUserId = postComment.getUserId();
            }else {
                targetUserId = commentRelation.getUserId();
            }

            String targetName = userService.getById(targetUserId).getUserName();
            commentVO.setTargetName(targetName);
            commentVO.setUserName(userName);
            return commentVO;
        }).collect(Collectors.toList());
        commentVOPage.setRecords(commentVOList);
        return commentVOPage;
    }

    @Override
    public QueryWrapper<CommentRelation> getQueryWrapper(CommentQueryRequest commentQueryRequest) {
        QueryWrapper<CommentRelation> queryWrapper = new QueryWrapper<>();
        if (commentQueryRequest == null) {
            return queryWrapper;
        }
        Long id = commentQueryRequest.getId();
        String sonId = commentQueryRequest.getSonId();
        String targetId = commentQueryRequest.getTargetId();
        Long postId = commentQueryRequest.getPostId();
        String content = commentQueryRequest.getContent();
        Long userId = commentQueryRequest.getUserId();
        String sortField = commentQueryRequest.getSortField();
        String sortOrder = commentQueryRequest.getSortOrder();

        // 拼接查询条件
        queryWrapper.eq(ObjectUtils.isNotEmpty(id), "id", id);
        queryWrapper.eq(ObjectUtils.isNotEmpty(postId), "postId", postId);
        queryWrapper.eq(ObjectUtils.isNotEmpty(userId), "userId", userId);
        queryWrapper.like(StringUtils.isNotBlank(content), "content", content);
        queryWrapper.like(StringUtils.isNotBlank(sonId), "sonId", sonId);
        queryWrapper.eq("isDelete", false);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);

        return queryWrapper;
    }

}




