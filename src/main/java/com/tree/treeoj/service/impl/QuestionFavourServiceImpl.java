package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.model.entity.QuestionFavour;
import com.tree.treeoj.service.QuestionFavourService;
import com.tree.treeoj.mapper.QuestionFavourMapper;
import org.springframework.stereotype.Service;

/**
* @author 11818
* @description 针对表【question_favour(题目收藏)】的数据库操作Service实现
* @createDate 2024-02-25 18:43:42
*/
@Service
public class QuestionFavourServiceImpl extends ServiceImpl<QuestionFavourMapper, QuestionFavour>
    implements QuestionFavourService{

}




