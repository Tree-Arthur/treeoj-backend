package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.constant.CommonConstant;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.judge.JudgeService;
import com.tree.treeoj.model.dto.questionsubmit.QuestionSubmitAddRequest;
import com.tree.treeoj.model.dto.questionsubmit.QuestionSubmitQueryRequest;
import com.tree.treeoj.model.entity.Question;
import com.tree.treeoj.model.entity.QuestionSubmit;
import com.tree.treeoj.model.entity.User;
import com.tree.treeoj.model.enums.QuestionSubmitLanguageEnum;
import com.tree.treeoj.model.enums.QuestionSubmitStatusEnum;
import com.tree.treeoj.model.vo.QuestionSubmitVO;
import com.tree.treeoj.model.vo.QuestionVO;
import com.tree.treeoj.model.vo.UserVO;
import com.tree.treeoj.service.QuestionService;
import com.tree.treeoj.service.QuestionSubmitService;
import com.tree.treeoj.mapper.QuestionSubmitMapper;
import com.tree.treeoj.service.UserService;
import com.tree.treeoj.utils.SqlUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @author Tree Arthur
 * @description 针对表【question_submit(题目提交)】的数据库操作Service实现
 * @createDate 2023-12-19 02:18:22
 */
@Service
public class QuestionSubmitServiceImpl extends ServiceImpl<QuestionSubmitMapper, QuestionSubmit>
        implements QuestionSubmitService {

    @Resource
    private QuestionService questionService;
    @Resource
    @Lazy
    private JudgeService judgeService;
    @Resource
    private UserService userService;

    /**
     * 提交题目
     *
     * @param questionSubmitAddRequest
     * @param loginUser
     * @return
     */
    @Override
    public long doQuestionSubmit(QuestionSubmitAddRequest questionSubmitAddRequest, User loginUser) {
        //TODO 校验编程语言是否合法
        String submitLanguage = questionSubmitAddRequest.getSubmitLanguage();
        QuestionSubmitLanguageEnum languageEnum = QuestionSubmitLanguageEnum.getEnumByValue(submitLanguage);
        if (languageEnum == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "编程语言错误");
        }
        long questionId = questionSubmitAddRequest.getQuestionId();
        // 判断实体是否存在，根据类别获取实体
        Question question = questionService.getById(questionId);
        if (question == null) {
            throw new BusinessException(ErrorCode.NOT_FOUND_ERROR);
        }
        // 是否已提交题目
        long userId = loginUser.getId();
        //每个用户串行提交题目
        QuestionSubmit questionSubmit = new QuestionSubmit();//创建对象
        questionSubmit.setUserId(userId);
        questionSubmit.setQuestionId(questionId);//设置题目ID
        questionSubmit.setSubmitCode(questionSubmitAddRequest.getSubmitCode());//设置提交代码
        questionSubmit.setSubmitLanguage(submitLanguage);//设置语言
        questionSubmit.setSubmitState(QuestionSubmitStatusEnum.WAITING.getValue());//设置初始状态
        questionSubmit.setJudgeInfo("{}");//
        boolean save = this.save(questionSubmit);
        if (!save) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "题目输入失败");
        }
        Long questionSubmitId = questionSubmit.getId();
        // 异步执行判题服务
        CompletableFuture.runAsync(() -> {
            judgeService.doJudge(questionSubmitId);
        });
        return questionSubmitId;
    }

    /**
     * 获取查询包装类（用户根据哪些字段查询，根据前端传来的请求对象）
     *
     * @param questionSubmitQueryRequest
     * @return
     */
    @Override
    public QueryWrapper<QuestionSubmit> getQueryWrapper(QuestionSubmitQueryRequest questionSubmitQueryRequest) {
        //获取前端发送的请求对象属性
        Long questionId = questionSubmitQueryRequest.getQuestionId();
        Long userId = questionSubmitQueryRequest.getUserId();
        String submitLanguage = questionSubmitQueryRequest.getSubmitLanguage();
        Integer submitState = questionSubmitQueryRequest.getSubmitState();
        String sortOrder = questionSubmitQueryRequest.getSortOrder();
        String sortField = questionSubmitQueryRequest.getSortField();

        //构建条件查询构造器
        QueryWrapper<QuestionSubmit> queryWrapper = new QueryWrapper<>();
        if (questionSubmitQueryRequest == null) return queryWrapper;//如果为空直接返回
        // 拼接查询条件
        queryWrapper.eq(ObjectUtils.isNotEmpty(submitLanguage), "submitLanguage", submitLanguage);//提交语言
        queryWrapper.eq(ObjectUtils.isNotEmpty(questionId), "questionId", questionId);//题目Id
        queryWrapper.eq(ObjectUtils.isNotEmpty(userId), "userId", userId);//用户id

        queryWrapper.eq(QuestionSubmitStatusEnum.getEnumByValue(submitState) != null, "submitState", submitState);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);
        queryWrapper.eq("isDelete", false);//未逻辑删除的

        return queryWrapper;
    }

    /**
     * 获取查询封装类（单个）
     *
     * @param questionSubmit
     * @param loginUser
     * @return
     */
    @Override
    public QuestionSubmitVO getQuestionSubmitVO(QuestionSubmit questionSubmit, User loginUser) {
        QuestionSubmitVO questionSubmitVO = QuestionSubmitVO.objToVo(questionSubmit);

        // 脱敏：仅本人和管理员能看见自己（提交 userId 和登录用户 id 不同）提交的代码
        Long userId = loginUser.getId();
        //处理脱敏 提交不是本人 并 登录的不是管理
        if (!questionSubmitVO.getUserId().equals(userId) && !userService.isAdmin(loginUser)) {
            questionSubmitVO.setSubmitCode(null);//提交空
        }
        UserVO userVO = userService.getUserVO(loginUser);
        questionSubmitVO.setUserVO(userVO);
        Question question = questionService.getById(questionSubmit.getQuestionId());
        QuestionVO questionVO = questionService.getQuestionVO(question,loginUser);
        questionSubmitVO.setQuestionVO(questionVO);
        return questionSubmitVO;
    }

    /**
     * 获取查询脱敏信息
     *
     * @param questionSubmitPage 题目提交分页
     * @param loginUser          直接获取到用户信息，减少查询数据库
     * @return
     */
    @Override
    public Page<QuestionSubmitVO> getQuestionSubmitVOPage(Page<QuestionSubmit> questionSubmitPage, User loginUser) {
        List<QuestionSubmit> questionSubmitList = questionSubmitPage.getRecords();//获取分页信息
        long current = questionSubmitPage.getCurrent();//当前页
        long size = questionSubmitPage.getSize();//
        long total = questionSubmitPage.getTotal();//总量

        Page<QuestionSubmitVO> questionSubmitVOPage = new Page<>(current, size, total);//分页对象
        if (CollectionUtils.isEmpty(questionSubmitList)) {//判空
            return questionSubmitVOPage;
        }

        //stream 流 map映射(题目提交映射)脱敏  合并列表
        List<QuestionSubmitVO> questionSubmitVOList = questionSubmitList.stream()
                .map(questionSubmit -> getQuestionSubmitVO(questionSubmit, loginUser))
                .collect(Collectors.toList());
        questionSubmitVOPage.setRecords(questionSubmitVOList);//设置题目提交分页视图 分页信息

        return questionSubmitVOPage;
    }


}




