package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.constant.CommonConstant;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.exception.ThrowUtils;
import com.tree.treeoj.model.dto.question.QuestionRelationQueryRequest;
import com.tree.treeoj.model.entity.*;
import com.tree.treeoj.model.vo.QuestionBankVO;
import com.tree.treeoj.model.vo.QuestionRelationVO;
import com.tree.treeoj.model.vo.QuestionVO;
import com.tree.treeoj.service.QuestionBankService;
import com.tree.treeoj.service.QuestionRelationService;
import com.tree.treeoj.mapper.QuestionRelationMapper;
import com.tree.treeoj.service.QuestionService;
import com.tree.treeoj.service.UserService;
import com.tree.treeoj.utils.SqlUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
* @author 11818
* @description 针对表【question_relation(题库关联题目)】的数据库操作Service实现
* @createDate 2024-03-14 11:37:03
*/
@Service
public class QuestionRelationServiceImpl extends ServiceImpl<QuestionRelationMapper, QuestionRelation>
    implements QuestionRelationService{

    @Resource
    private QuestionBankService questionBankService;
    @Resource
    private QuestionService questionService;
    @Resource
    private UserService userService;
    @Override
    public void validQuestionRelation(QuestionRelation questionRelation, boolean add) {
        if (questionRelation == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long questionBankId = questionRelation.getQuestionBankId();
        Long questionId = questionRelation.getQuestionId();
        // 创建时，参数不能为空
        if (add) {
            ThrowUtils.throwIf(ObjectUtils.anyNull(questionBankId, questionId), ErrorCode.PARAMS_ERROR);
        }
    }

    @Override
    public QueryWrapper<QuestionRelation> getQueryWrapper(QuestionRelationQueryRequest questionRelationQueryRequest) {
        Long id = questionRelationQueryRequest.getId();
        Long questionBankId = questionRelationQueryRequest.getQuestionBankId();
        Long questionId = questionRelationQueryRequest.getQuestionId();
        String title = questionRelationQueryRequest.getTitle();
        Long userId = questionRelationQueryRequest.getUserId();
        String content = questionRelationQueryRequest.getContent();
        List<String> tags = questionRelationQueryRequest.getTags();
        String searchText = questionRelationQueryRequest.getSearchText();
        String sortField = questionRelationQueryRequest.getSortField();
        String sortOrder = questionRelationQueryRequest.getSortOrder();


        QueryWrapper<QuestionRelation> queryWrapper = new QueryWrapper<>();
        if (questionRelationQueryRequest == null) {
            return queryWrapper;
        }

        if (CollectionUtils.isNotEmpty(tags)) {
            for (String tag : tags) {
                queryWrapper.like("tags", "\"" + tag + "\"");
            }
        }
        // 拼接查询条件
        queryWrapper.eq(id != null, "id", id);
        queryWrapper.eq(userId != null, "userId", userId);
        queryWrapper.eq(questionBankId != null, "questionBankId", questionBankId);
        queryWrapper.eq( questionId != null, "questionId", questionId);
        queryWrapper.eq(StringUtils.isNotBlank(title), "title", title);
        queryWrapper.eq(StringUtils.isNotBlank(content), "content", content);
        queryWrapper.like(StringUtils.isNotBlank(searchText), "searchText", searchText);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);
        return queryWrapper;

    }

    @Override
    public Page<QuestionRelationVO> getQuestionRelationVOPage(Page<QuestionRelation> questionRelationPage, User loginUser) {
        List<QuestionRelation> questionRelationList = questionRelationPage.getRecords();
        Page<QuestionRelationVO> questionRelationVOPage = new Page<>(questionRelationPage.getCurrent(), questionRelationPage.getSize(), questionRelationPage.getTotal());
        if (CollectionUtils.isEmpty(questionRelationList)) {
            return questionRelationVOPage;
        }
        List<QuestionRelationVO> questionRelationVOList = questionRelationList.stream().map(questionRelation -> {
            QuestionRelationVO questionRelationVO = QuestionRelationVO.objToVo(questionRelation);
            QuestionBank questionBank = questionBankService.getById(questionRelation.getQuestionBankId());
            questionRelationVO.setQuestionBankVO(QuestionBankVO.objToVo(questionBank));
            Question question = questionService.getById(questionRelation.getQuestionId());
            QuestionVO questionVO = questionService.getQuestionVO(question, loginUser);
            questionRelationVO.setQuestionVO(questionVO);

            return questionRelationVO;
        }).collect(Collectors.toList());

        questionRelationVOPage.setRecords(questionRelationVOList);
        return questionRelationVOPage;
    }
}




