package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.model.entity.QuestionThumb;
import com.tree.treeoj.service.QuestionThumbService;
import com.tree.treeoj.mapper.QuestionThumbMapper;
import org.springframework.stereotype.Service;

/**
* @author 11818
* @description 针对表【question_thumb(题目点赞)】的数据库操作Service实现
* @createDate 2024-02-25 18:43:50
*/
@Service
public class QuestionThumbServiceImpl extends ServiceImpl<QuestionThumbMapper, QuestionThumb>
    implements QuestionThumbService{

}




