package com.tree.treeoj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.constant.CommonConstant;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.exception.ThrowUtils;
import com.tree.treeoj.model.dto.comment.PostCommentQueryRequest;
import com.tree.treeoj.model.entity.*;
import com.tree.treeoj.mapper.PostCommentMapper;
import com.tree.treeoj.model.vo.PostCommentVO;
import com.tree.treeoj.model.vo.PostVO;
import com.tree.treeoj.model.vo.UserVO;
import com.tree.treeoj.service.PostCommentService;
import com.tree.treeoj.service.UserService;
import com.tree.treeoj.utils.SqlUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 11818
 * @description 针对表【post_comment(帖子评论)】的数据库操作Service实现
 * @createDate 2024-02-26 12:22:48
 */
@Service
public class PostCommentServiceImpl extends ServiceImpl<PostCommentMapper, PostComment>
        implements PostCommentService {

    @Resource
    private UserService userService;

    @Override
    public void validPostComment(PostComment postComment, boolean add) {
        if (postComment == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        String fatherId = postComment.getFatherId();
        Long postId = postComment.getPostId();
        String content = postComment.getContent();
        Long userId = postComment.getUserId();
        // 创建时，参数不能为空
        if (add) {
            ThrowUtils.throwIf(StringUtils.isAnyBlank(content), ErrorCode.PARAMS_ERROR);
            ThrowUtils.throwIf(StringUtils.isAnyBlank(fatherId), ErrorCode.PARAMS_ERROR);
            ThrowUtils.throwIf(postId == null, ErrorCode.PARAMS_ERROR);
            ThrowUtils.throwIf(userId == null, ErrorCode.PARAMS_ERROR);
        }
        // 有参数则校验
        if (StringUtils.isNotBlank(content) && content.length() > 80) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "评论内容过长");
        }
    }

    @Override
    public PostCommentVO getPostCommentVO(PostComment postComment, HttpServletRequest request) {
        PostCommentVO postCommentVO = PostCommentVO.objToVo(postComment);
        return postCommentVO;
    }

    @Override
    public Page<PostCommentVO> getPostCommentVOPage(Page<PostComment> postCommentPage, HttpServletRequest request) {
        List<PostComment> postCommentList = postCommentPage.getRecords();
        Page<PostCommentVO> postCommentVOPage = new Page<>(postCommentPage.getCurrent(), postCommentPage.getSize(), postCommentPage.getTotal());
        if (CollectionUtils.isEmpty(postCommentList)) {
            return postCommentVOPage;
        }
        // 填充信息
        List<PostCommentVO> postCommentVOList = postCommentList.stream().map(postComment -> {
            PostCommentVO postCommentVO = PostCommentVO.objToVo(postComment);
            UserVO userVO = userService.getUserVO(userService.getById(postComment.getUserId()));
            postCommentVO.setUserVO(userVO);
            return postCommentVO;
        }).collect(Collectors.toList());
        postCommentVOPage.setRecords(postCommentVOList);
        return postCommentVOPage;
    }

    @Override
    public QueryWrapper<PostComment> getQueryWrapper(PostCommentQueryRequest postCommentQueryRequest) {
        QueryWrapper<PostComment> queryWrapper = new QueryWrapper<>();
        if (postCommentQueryRequest == null) {
            return queryWrapper;
        }
        Long id = postCommentQueryRequest.getId();
        String fatherId = postCommentQueryRequest.getFatherId();
        Long postId = postCommentQueryRequest.getPostId();
        String content = postCommentQueryRequest.getContent();
        Long userId = postCommentQueryRequest.getUserId();
        String sortField = postCommentQueryRequest.getSortField();
        String sortOrder = postCommentQueryRequest.getSortOrder();

        // 拼接查询条件
        queryWrapper.like(StringUtils.isNotBlank(content), "content", content);
        queryWrapper.like(StringUtils.isNotBlank(fatherId), "fatherId", fatherId);
        queryWrapper.eq(ObjectUtils.isNotEmpty(id), "id", id);
        queryWrapper.eq(ObjectUtils.isNotEmpty(postId), "postId", postId);
        queryWrapper.eq(ObjectUtils.isNotEmpty(userId), "userId", userId);
        queryWrapper.eq("isDelete", false);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);
        return queryWrapper;
    }

}




