package com.tree.treeoj.model.vo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tree.treeoj.model.entity.QuestionBank;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 11:47
 * @Package: com.tree.treeoj.model.vo
 * @Description: 题库Vo
 * @version: 1.0
 */
@Data
public class QuestionBankVO implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 题库名称
     */
    private String title;

    /**
     * 创建题目用户 id
     */
    private Long userId;

    /**
     * 题库内容
     */
    private String content;

    /**
     * 标签列表（json 数组）
     */
    private List<String> tagList;

    /**
     * 点赞数
     */
    private Integer thumbNum;

    /**
     * 收藏数
     */
    private Integer favourNum;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否已点赞
     */
    private Boolean hasThumb;

    /**
     * 是否已收藏
     */
    private Boolean hasFavour;

    private static final long serialVersionUID = 1L;
    private final static Gson GSON = new Gson();
    /**
     * 包装类转对象
     *
     * @param questionBankVO
     * @return
     */
    public static QuestionBank voToObj(QuestionBankVO questionBankVO) {
        if (questionBankVO == null) {
            return null;
        }
        QuestionBank questionBank = new QuestionBank();
        BeanUtils.copyProperties(questionBankVO, questionBank);
        List<String> tagList = questionBankVO.getTagList();
        if (tagList != null) {
            questionBank.setTags(GSON.toJson(tagList));
        }
        return questionBank;
    }

    /**
     * 对象转包装类
     *
     * @param questionBank
     * @return
     */
    public static QuestionBankVO objToVo(QuestionBank questionBank) {
        if (questionBank == null) {
            return null;
        }
        QuestionBankVO questionBankVo = new QuestionBankVO();
        BeanUtils.copyProperties(questionBank, questionBankVo);
        questionBankVo.setTagList(GSON.fromJson(questionBank.getTags(), new TypeToken<List<String>>() {
        }.getType()));
        return questionBankVo;
    }
}
