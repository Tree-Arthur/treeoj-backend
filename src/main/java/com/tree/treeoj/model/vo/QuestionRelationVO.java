package com.tree.treeoj.model.vo;

import com.google.gson.Gson;
import com.tree.treeoj.model.entity.QuestionRelation;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : Tree Arthur
 * @date: 2024/3/15 3:06
 * @Package: com.tree.treeoj.model.vo
 * @Description: 题目关联Vo
 * @version: 1.0
 */
@Data
public class QuestionRelationVO implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 题库 id
     */
    private Long questionBankId;

    /**
     * 题目 id
     */
    private Long questionId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 题库Vo
     */
    private QuestionBankVO questionBankVO;

    /**
     * 题目Vo列表
     */
    private QuestionVO questionVO;

    private static final long serialVersionUID = 1L;
    private final static Gson GSON = new Gson();
    /**
     * 包装类转对象
     *
     * @param questionRelationVO
     * @return
     */
    public static QuestionRelation voToObj(QuestionRelationVO questionRelationVO) {
        if (questionRelationVO == null) {
            return null;
        }
        QuestionRelation questionRelation = new QuestionRelation();
        BeanUtils.copyProperties(questionRelationVO, questionRelation);
//        List<String> tagList = questionRelationVO.getTagList();
//        if (tagList != null) {
//            questionBank.setTags(GSON.toJson(tagList));
//        }
        return questionRelation;
    }

    /**
     * 对象转包装类
     *
     * @param questionRelation
     * @return
     */
    public static QuestionRelationVO objToVo(QuestionRelation questionRelation) {
        if (questionRelation == null) {
            return null;
        }
        QuestionRelationVO questionRelationVO = new QuestionRelationVO();
        BeanUtils.copyProperties(questionRelation, questionRelationVO);
//        questionRelationVO.setTagList(GSON.fromJson(questionRelation.getTags(), new TypeToken<List<String>>() {
//        }.getType()));
        return questionRelationVO;
    }
}
