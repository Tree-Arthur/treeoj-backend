package com.tree.treeoj.model.vo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tree.treeoj.model.entity.UserInfo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户其他信息
 */
@Data
public class UserInfoVO implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 创建用户 id
     */
    private Long userId;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态 0-在线 1注销
     */
    private Integer userStatus;

    /**
     * 标签 json 列表
     */
    private List<String> tagList;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
    private final static Gson GSON = new Gson();

    /**
     * 包装类转对象
     *
     * @param userInfoVO
     * @return
     */
    public static UserInfo voToObj(UserInfoVO userInfoVO) {
        if (userInfoVO == null) {
            return null;
        }
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userInfoVO, userInfo);//拷贝属性
        List<String> tagList = userInfoVO.getTagList();
        if (tagList != null) {
            userInfo.setTags(GSON.toJson(tagList));
        }

        return userInfo;
    }

    /**
     * 对象转包装类
     *
     * @param userInfo
     * @return
     */
    public static UserInfoVO objToVo(UserInfo userInfo) {
        if (userInfo == null) {
            return null;
        }
        UserInfoVO userInfoVO = new UserInfoVO();
        BeanUtils.copyProperties(userInfo, userInfoVO);
        userInfoVO.setTagList(GSON.fromJson(userInfo.getTags(), new TypeToken<List<String>>() {
        }.getType()));
        return userInfoVO;
    }
}