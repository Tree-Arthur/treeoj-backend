package com.tree.treeoj.model.vo;

import com.google.gson.Gson;
import com.tree.treeoj.model.entity.CommentRelation;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 帖子评论视图
 *
 */
@Data
public class CommentVO implements Serializable {

    private final static Gson GSON = new Gson();

    /**
     * id
     */
    private Long id;

    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 子级评论id(唯一)
     */
    private String sonId;

    /**
     * 评论的那条记录的id(判断父评论或子评论)
     */
    private String targetId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论用户 id
     */
    private Long userId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 目标用户名
     */
    private String targetName;

    private static final long serialVersionUID = 1L;

    /**
     * 包装类转对象
     *
     * @param commentVO
     * @return
     */
    public static CommentRelation voToObj(CommentVO commentVO) {
        if (commentVO == null) {
            return null;
        }
        CommentRelation commentRelation = new CommentRelation();
        BeanUtils.copyProperties(commentVO, commentRelation);
        return commentRelation;
    }

    /**
     * 对象转包装类
     *
     * @param commentRelation
     * @return
     */
    public static CommentVO objToVo(CommentRelation commentRelation) {
        if (commentRelation == null) {
            return null;
        }
        CommentVO commentVO = new CommentVO();
        BeanUtils.copyProperties(commentRelation, commentVO);
        return commentVO;
    }
}
