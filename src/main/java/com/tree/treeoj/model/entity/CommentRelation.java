package com.tree.treeoj.model.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 评论关联
 * @TableName comment_relation
 */
@TableName(value ="comment_relation")
@Data
public class CommentRelation implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 子级评论id(唯一)
     */
    private String sonId;

    /**
     * 评论的那条记录的id(判断父评论或子评论)
     */
    private String targetId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论用户 id
     */
    private Long userId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}