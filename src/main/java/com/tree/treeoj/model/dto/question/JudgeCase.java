package com.tree.treeoj.model.dto.question;

import lombok.Data;

/**
 * @author : Tree Arthur
 * @date: 2023/12/19 3:08
 * @Package: com.tree.treeoj.model.entity
 * @Description: 题目用例
 * @version: 1.0
 */
@Data
public class JudgeCase {
    /**
     * 输入用例
     */
    private String input;
    /**
     * 输出用例
     */
    private String output;
}
