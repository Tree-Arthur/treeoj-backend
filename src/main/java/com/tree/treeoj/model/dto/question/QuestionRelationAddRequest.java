package com.tree.treeoj.model.dto.question;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 13:30
 * @Package: com.tree.treeoj.model.dto.question
 * @Description: TODO
 * @version: 1.0
 */
@Data
public class QuestionRelationAddRequest implements Serializable {

    /**
     * 题库 id
     */
    private Long questionBankId;

    /**
     * 题目 id
     */
    private Long questionId;

    private static final long serialVersionUID = 1L;
}
