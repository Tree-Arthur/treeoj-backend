package com.tree.treeoj.model.dto.comment;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 评论关联
 * @TableName comment_relation
 */
@Data
public class CommentRelationAddRequest implements Serializable {

    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 子级评论id(唯一)
     */
    private String sonId;

    /**
     * 评论的那条记录的id(判断父评论或子评论)
     */
    private String targetId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论用户 id
     */
    private Long userId;

    private static final long serialVersionUID = 1L;
}