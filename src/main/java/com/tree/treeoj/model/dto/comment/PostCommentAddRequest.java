package com.tree.treeoj.model.dto.comment;

import lombok.Data;

import java.io.Serializable;

/**
 * 帖子评论添加请求
 */
@Data
public class PostCommentAddRequest implements Serializable {

    /**
     * 父级评论id(唯一)
     */
    private String fatherId;

    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论用户 id
     */
    private Long userId;

    private static final long serialVersionUID = 1L;
}