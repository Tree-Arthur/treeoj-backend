package com.tree.treeoj.model.dto.questionbank;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @author : Tree Arthur
 * @date: 2024/3/14 11:43
 * @Package: com.tree.treeoj.model.dto.questionbank
 * @Description: 添加题库请求
 * @version: 1.0
 */
@Data
public class QuestionBankAddRequest implements Serializable {

    /**
     * 题库名称
     */
    private String title;

    /**
     * 题库内容
     */
    private String content;

    /**
     * 标签列表（json 数组）
     */
    private List<String> tags;

    private static final long serialVersionUID = 1L;
}
