package com.tree.treeoj.model.dto.question;

import com.tree.treeoj.common.PageRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 14:12
 * @Package: com.tree.treeoj.model.dto.question
 * @Description: 题目关系搜索请求
 * @version: 1.0
 */
@Data
public class QuestionRelationQueryRequest extends PageRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 题库 id
     */
    private Long questionBankId;

    /**
     * 题目 id
     */
    private Long questionId;

    /**
     * 题目名称
     */
    private String title;

    /**
     * 创建题目用户 id
     */
    private Long userId;

    /**
     * 题目内容
     */
    private String content;

    /**
     * 标签列表（json 数组）
     */
    private List<String> tags;

    /**
     * 搜索词
     */
    private String searchText;

    private static final long serialVersionUID = 1L;
}
