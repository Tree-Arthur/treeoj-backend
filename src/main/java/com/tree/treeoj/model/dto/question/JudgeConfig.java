package com.tree.treeoj.model.dto.question;

import lombok.Data;

/**
 * @author : Tree Arthur
 * @date: 2023/12/19 3:11
 * @Package: com.tree.treeoj.model.entity
 * @Description: 配置
 * @version: 1.0
 */
@Data
public class JudgeConfig {

    /**
     * 时间限制 ms
     */
    private long timeLimit;

    /**
     * 内存限制 kb
     */
    private long memoryLimit;

    /**
     * 堆栈限制 kb
     */
    private long stackLimit;

}
