package com.tree.treeoj.model.dto.comment;

import lombok.Data;

import java.io.Serializable;

/**
 * @author : Tree Arthur
 * @date: 2024/2/26 13:02
 * @Package: com.tree.treeoj.model.dto.comment
 * @Description: 帖子评论更新请求
 * @version: 1.0
 */
@Data
public class PostCommentUpdateRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 评论内容
     */
    private String content;

    private static final long serialVersionUID = 1L;
}
