package com.tree.treeoj.model.dto.questionbank;

import com.tree.treeoj.common.PageRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 11:55
 * @Package: com.tree.treeoj.model.dto.questionbank
 * @Description: 题库搜索请求
 * @version: 1.0
 */
@Data
public class QuestionBankQueryRequest extends PageRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 题库名称
     */
    private String title;

    /**
     * 创建题目用户 id
     */
    private Long userId;

    /**
     * 题库内容
     */
    private String content;

    /**
     * 标签列表（json 数组）
     */
    private List<String> tags;

    /**
     * 搜索词
     */
    private String searchText;

    private static final long serialVersionUID = 1L;
}
