package com.tree.treeoj.model.dto.comment;

import com.tree.treeoj.common.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 帖子评论查询请求
 *
 * @author Tree Arthur
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PostCommentQueryRequest extends PageRequest implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 父级评论id(唯一)
     */
    private String fatherId;

    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建用户 id
     */
    private Long userId;

    private static final long serialVersionUID = 1L;
}