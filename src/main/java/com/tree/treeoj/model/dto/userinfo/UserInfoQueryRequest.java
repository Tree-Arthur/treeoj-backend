package com.tree.treeoj.model.dto.userinfo;

import com.tree.treeoj.common.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户其他信息查询请求
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserInfoQueryRequest extends PageRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 创建用户 id
     */
    private Long userId;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态 0-正常 1注销
     */
    private Integer userStatus;

    /**
     * 标签 json 列表
     */
    private String tags;

    private static final long serialVersionUID = 1L;
}