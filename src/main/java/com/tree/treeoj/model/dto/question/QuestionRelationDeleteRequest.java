package com.tree.treeoj.model.dto.question;

import lombok.Data;

import java.io.Serializable;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 13:30
 * @Package: com.tree.treeoj.model.dto.question
 * @Description: 删除请求
 * @version: 1.0
 */
@Data
public class QuestionRelationDeleteRequest implements Serializable {

    /**
     * 题库 id
     */
    private Long questionBankId;

    /**
     * 题目 id
     */
    private Long questionId;

    private static final long serialVersionUID = 1L;
}
