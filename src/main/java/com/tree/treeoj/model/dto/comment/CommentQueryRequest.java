package com.tree.treeoj.model.dto.comment;

import com.tree.treeoj.common.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 帖子二级评论查询请求
 *
 * @author Tree Arthur
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CommentQueryRequest extends PageRequest implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 子级评论id(唯一)
     */
    private String sonId;

    /**
     * 评论的那条记录的id(判断父评论或子评论)
     */
    private String targetId;

    /**
     * 帖子id
     */
    private Long postId;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建用户 id
     */
    private Long userId;

    private static final long serialVersionUID = 1L;
}