package com.tree.treeoj.model.dto.questionbank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 12:04
 * @Package: com.tree.treeoj.model.dto.questionbank
 * @Description: 题库编辑请求
 * @version: 1.0
 */
@Data
public class QuestionBankEditRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 题库名称
     */
    private String title;

    /**
     * 创建题目用户 id
     */
    private Long userId;

    /**
     * 题库内容
     */
    private String content;

    /**
     * 标签列表（json 数组）
     */
    private List<String> tags;

    private static final long serialVersionUID = 1L;
}
