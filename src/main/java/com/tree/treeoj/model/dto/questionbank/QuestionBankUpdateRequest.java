package com.tree.treeoj.model.dto.questionbank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2024/3/14 11:53
 * @Package: com.tree.treeoj.model.dto.questionbank
 * @Description: 题库更新请求
 * @version: 1.0
 */
@Data
public class QuestionBankUpdateRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    /**
     * 题库名称
     */
    private String title;

    /**
     * 题库内容
     */
    private String content;

    /**
     * 标签列表（json 数组）
     */
    private List<String> tags;

    private static final long serialVersionUID = 1L;
}
