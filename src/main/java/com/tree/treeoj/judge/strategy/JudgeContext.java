package com.tree.treeoj.judge.strategy;

import com.tree.treeoj.model.dto.question.JudgeCase;
import com.tree.treeoj.judge.codesandbox.model.JudgeInfo;
import com.tree.treeoj.model.entity.Question;
import com.tree.treeoj.model.entity.QuestionSubmit;
import lombok.Data;

import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2023/12/23 0:24
 * @Package: com.tree.treeoj.judge.strategy
 * @Description: 上下文（用于定义在策略中传递的参数）
 * @version: 1.0
 */
@Data
public class JudgeContext {

    /**
     * 判题信息
     */
    private JudgeInfo judgeInfo;
    /**
     * 输入用例列表
     */
    private List<String> inputList;
    /**
     * 输出用例列表
     */
    private List<String> outputList;
    /**
     * 判题用例列表
     */
    private List<JudgeCase> judgeCaseList;
    /**
     * 题目
     */
    private Question question;
    /**
     * 题目提交
     */
    private QuestionSubmit questionSubmit;
}
