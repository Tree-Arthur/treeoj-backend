package com.tree.treeoj.judge.strategy;

import com.tree.treeoj.judge.codesandbox.model.JudgeInfo;

/**
 * @author : Tree Arthur
 * @date: 2023/12/23 0:22
 * @Package: com.tree.treeoj.judge.strategy
 * @Description: 判题策略
 * @version: 1.0
 */
public interface JudgeStrategy {
    /**
     * 执行判题
     * @param judgeContext 判题上下文
     * @return
     */
    JudgeInfo doJudge(JudgeContext judgeContext);
}
