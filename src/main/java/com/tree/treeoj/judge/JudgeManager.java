package com.tree.treeoj.judge;

import com.tree.treeoj.judge.strategy.DefaultJudgeStrategy;
import com.tree.treeoj.judge.strategy.JavaLanguageJudgeStrategy;
import com.tree.treeoj.judge.strategy.JudgeContext;
import com.tree.treeoj.judge.strategy.JudgeStrategy;
import com.tree.treeoj.judge.codesandbox.model.JudgeInfo;
import com.tree.treeoj.model.entity.QuestionSubmit;
import org.springframework.stereotype.Service;

/**
 * @author : Tree Arthur
 * @date: 2023/12/23 0:53
 * @Package: com.tree.treeoj.judge.strategy
 * @Description: 判题管理（简化调用）
 * @version: 1.0
 */
@Service
public class JudgeManager {

    /**
     * 执行判题
     *
     * @param judgeContext
     * @return
     */
     JudgeInfo doJudge(JudgeContext judgeContext) {
        QuestionSubmit questionSubmit = judgeContext.getQuestionSubmit();
        String language = questionSubmit.getSubmitLanguage();
        JudgeStrategy judgeStrategy = new DefaultJudgeStrategy();
        if ("java".equals(language)) {
            judgeStrategy = new JavaLanguageJudgeStrategy();
        }
        // 继续判断提交的语言，创建对应语言的判题策略
        return judgeStrategy.doJudge(judgeContext);
    }
}
