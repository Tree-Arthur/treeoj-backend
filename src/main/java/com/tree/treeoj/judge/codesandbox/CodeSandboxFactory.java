package com.tree.treeoj.judge.codesandbox;

import com.tree.treeoj.judge.codesandbox.impl.ExampleCodeSandbox;
import com.tree.treeoj.judge.codesandbox.impl.RemoteCodeSandbox;
import com.tree.treeoj.judge.codesandbox.impl.ThirdPartyCodeSandbox;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 22:35
 * @Package: com.tree.treeoj.judge.codesandbox
 * @Description: 代码沙盒工厂(根据字符串参数创建指定的代码沙盒实例)
 * @version: 1.0
 */
public class CodeSandboxFactory {
    /**
     * 创建代码沙盒示例
     * @param type 沙盒类型
     * @return
     */
    public static CodeSandbox newInstance(String type){
        switch (type){
            case "example":
                return new ExampleCodeSandbox();
            case "remote":
                return new RemoteCodeSandbox();
            case "thirdParty":
                return new ThirdPartyCodeSandbox();
            default:
                return new ExampleCodeSandbox();
        }
    }
}
