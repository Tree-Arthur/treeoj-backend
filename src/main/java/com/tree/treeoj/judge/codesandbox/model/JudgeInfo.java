package com.tree.treeoj.judge.codesandbox.model;

import lombok.Data;

/**
 * @author : Tree Arthur
 * @date: 2023/12/19 3:10
 * @Package: com.tree.treeoj.model.entity
 * @Description: 判题信息
 * @version: 1.0
 */
@Data
public class JudgeInfo {
    /**
     * 程序执行信息
     */
    private String message;

    /**
     * 消耗时间 ms
     */
    private Long time;

    /**
     * 消耗内存 kb
     */
    private Long memory;
}
