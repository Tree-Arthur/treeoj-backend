package com.tree.treeoj.judge.codesandbox;

import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeRequest;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeResponse;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 22:08
 * @Package: com.tree.treeoj.judge.codesandbox
 * @Description: 代码沙盒
 * @version: 1.0
 */
public interface CodeSandbox {
    /**
     * 执行代码
     * @param executeCodeRequest
     * @return
     */
    ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest);
}
