package com.tree.treeoj.judge.codesandbox;

import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeRequest;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 22:58
 * @Package: com.tree.treeoj.judge.codesandbox
 * @Description: 代码沙盒代理类
 * @version: 1.0
 */
@Slf4j
public class CodeSandboxProxy implements CodeSandbox {

    private final CodeSandbox codeSandbox;

    public CodeSandboxProxy(CodeSandbox codeSandbox) {
        this.codeSandbox = codeSandbox;
    }

    @Override
    public ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest) {
        //在调用代码沙箱前，输出请求参数日志
        log.info("代码沙盒请求信息: " + executeCodeRequest);

        ExecuteCodeResponse executeCodeResponse = codeSandbox.executeCode(executeCodeRequest);

        //代码沙箱调用后，输出响应结果日志，便于管理员去分析
        log.info("代码沙盒响应信息: " + executeCodeResponse);
        return executeCodeResponse;
    }
}
