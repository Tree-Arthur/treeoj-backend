package com.tree.treeoj.judge.codesandbox.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 22:11
 * @Package: com.tree.treeoj.judge.codesandbox
 * @Description: 代码响应
 * @version: 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExecuteCodeResponse {
    /**
     * 输出
     */
    private List<String> outputList;

    /**
     * 接口信息
     */
    private String message;

    /**
     * 执行状态
     */
    private String status;
    /**
     *
     */
    private JudgeInfo judgeInfo;
}
