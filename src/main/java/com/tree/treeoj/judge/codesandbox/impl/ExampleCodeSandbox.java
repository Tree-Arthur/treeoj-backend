package com.tree.treeoj.judge.codesandbox.impl;

import com.tree.treeoj.judge.codesandbox.CodeSandbox;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeRequest;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeResponse;
import com.tree.treeoj.judge.codesandbox.model.JudgeInfo;
import com.tree.treeoj.model.enums.QuestionSubmitStatusEnum;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 22:18
 * @Package: com.tree.treeoj.judge.codesandbox.impl
 * @Description: 示例代码沙盒(仅为了跑通业务流程)
 * @version: 1.0
 */
@Slf4j
public class ExampleCodeSandbox implements CodeSandbox {
    @Override
    public ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest) {

        List<String> inputList = executeCodeRequest.getInputList();

        JudgeInfo judgeInfo = new JudgeInfo();
        judgeInfo.setMessage("judgeInfo信息");
        judgeInfo.setTime(2304L);
        judgeInfo.setMemory(1000L);

        //设置响应
        ExecuteCodeResponse executeCodeResponse = new ExecuteCodeResponse();
        executeCodeResponse.setOutputList(inputList);
        executeCodeResponse.setMessage("示例代码沙盒");
        executeCodeResponse.setStatus(QuestionSubmitStatusEnum.SUCCEED.getText());
        executeCodeResponse.setJudgeInfo(judgeInfo);

        return executeCodeResponse;
    }
}
