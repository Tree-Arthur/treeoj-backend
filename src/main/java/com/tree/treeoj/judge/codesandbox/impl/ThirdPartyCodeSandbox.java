package com.tree.treeoj.judge.codesandbox.impl;

import com.tree.treeoj.judge.codesandbox.CodeSandbox;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeRequest;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeResponse;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 22:18
 * @Package: com.tree.treeoj.judge.codesandbox.impl
 * @Description: 第三方代码沙盒(调用网上现成沙盒)
 * @version: 1.0
 */
public class ThirdPartyCodeSandbox implements CodeSandbox {
    @Override
    public ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest) {
        System.out.println("第三方代码沙盒");

        return null;
    }
}
