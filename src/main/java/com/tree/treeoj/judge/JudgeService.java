package com.tree.treeoj.judge;

import com.tree.treeoj.model.entity.QuestionSubmit;
import org.springframework.stereotype.Service;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 23:31
 * @Package: com.tree.treeoj.judge
 * @Description: 判题服务
 * @version: 1.0
 */
public interface JudgeService {

    /**
     * 判题
     * @param questionSubmitId 提交题目Id
     * @return
     */
    QuestionSubmit doJudge(long questionSubmitId);
}
