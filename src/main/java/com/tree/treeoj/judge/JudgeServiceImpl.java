package com.tree.treeoj.judge;

import cn.hutool.json.JSONUtil;
import com.tree.treeoj.common.ErrorCode;
import com.tree.treeoj.exception.BusinessException;
import com.tree.treeoj.judge.codesandbox.CodeSandbox;
import com.tree.treeoj.judge.codesandbox.CodeSandboxFactory;
import com.tree.treeoj.judge.codesandbox.CodeSandboxProxy;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeRequest;
import com.tree.treeoj.judge.codesandbox.model.ExecuteCodeResponse;
import com.tree.treeoj.judge.strategy.JudgeContext;
import com.tree.treeoj.model.dto.question.JudgeCase;
import com.tree.treeoj.judge.codesandbox.model.JudgeInfo;
import com.tree.treeoj.model.entity.Question;
import com.tree.treeoj.model.entity.QuestionSubmit;
import com.tree.treeoj.model.enums.QuestionSubmitStatusEnum;
import com.tree.treeoj.service.QuestionService;
import com.tree.treeoj.service.QuestionSubmitService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Tree Arthur
 * @date: 2023/12/22 23:36
 * @Package: com.tree.treeoj.judge
 * @Description: 判题服务实现
 * @version: 1.0
 */
@Service
public class JudgeServiceImpl implements JudgeService {

    @Value("${codesandbox.type:example}")
    private String type;

    @Resource
    private QuestionService questionService;

    @Resource
    private QuestionSubmitService questionSubmitService;

    @Resource
    private JudgeManager judgeManager;

    @Override
    public QuestionSubmit doJudge(long questionSubmitId) {
        //1）传入题目的提交 id，获取到对应的题目、提交信息（包含代码、编程语言等）
        QuestionSubmit questionSubmit = questionSubmitService.getById(questionSubmitId);
        if (questionSubmit == null) {
            throw new BusinessException(ErrorCode.NOT_FOUND_ERROR, "题目提交信息不存在");
        }
        Long questionId = questionSubmit.getQuestionId();//获取题目ID
        Question question = questionService.getById(questionId);//根据ID获取题目信息
        if (question == null) {
            throw new BusinessException(ErrorCode.NOT_FOUND_ERROR, "题目不存在");
        }

        //2）如果题目提交状态不为等待中，就不用重复执行了
        if (!questionSubmit.getSubmitState().equals(QuestionSubmitStatusEnum.WAITING.getValue())) {
//            System.out.println("题目正在判题中");
            throw new BusinessException(ErrorCode.OPERATION_ERROR, "题目正在判题中");
        }

        //3）更改判题（题目提交）的状态为 “判题中”，防止重复执行，也能让用户即时看到状态
        QuestionSubmit questionSubmitUpdate = new QuestionSubmit();
        questionSubmitUpdate.setId(questionSubmitId);
        questionSubmitUpdate.setSubmitState(QuestionSubmitStatusEnum.RUNNING.getValue());
        boolean update = questionSubmitService.updateById(questionSubmitUpdate);
        if (!update) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "题目状态更新错误");
        }

        //4）调用沙箱，获取到执行结果
        CodeSandbox codeSandbox = CodeSandboxFactory.newInstance(type);//工厂模式
        codeSandbox = new CodeSandboxProxy(codeSandbox);//代理增强
        //参数赋值
        String submitCode = questionSubmit.getSubmitCode();
        String submitLanguage = questionSubmit.getSubmitLanguage();
        //获取输入用例
        String judgeCases = question.getJudgeCase();
        List<JudgeCase> judgeCaseList = JSONUtil.toList(judgeCases, JudgeCase.class);
        //stream流映 map映射 取出input 组合新的list
        List<String> inputList = judgeCaseList.stream().map(JudgeCase::getInput).collect(Collectors.toList());
        //建造者模式创建请求对象并初始化参数
        ExecuteCodeRequest executeCodeRequest = ExecuteCodeRequest.builder()
                .code(submitCode)
                .language(submitLanguage)
                .inputList(inputList)
                .build();
        //调用接口方法获取响应
        ExecuteCodeResponse executeCodeResponse = codeSandbox.executeCode(executeCodeRequest);
        List<String> outputList = executeCodeResponse.getOutputList();
        //5）根据沙箱的执行结果，设置题目的判题状态和信息
        JudgeContext judgeContext = new JudgeContext();
        judgeContext.setJudgeInfo(executeCodeResponse.getJudgeInfo());
        judgeContext.setInputList(inputList);
        judgeContext.setOutputList(outputList);
        judgeContext.setJudgeCaseList(judgeCaseList);
        judgeContext.setQuestion(question);
        judgeContext.setQuestionSubmit(questionSubmit);
        JudgeInfo judgeInfo = judgeManager.doJudge(judgeContext);
        //修改数据库中的判题结果
        questionSubmitUpdate = new QuestionSubmit();
        questionSubmitUpdate.setId(questionSubmitId);
        questionSubmitUpdate.setSubmitState(QuestionSubmitStatusEnum.SUCCEED.getValue());
        questionSubmitUpdate.setJudgeInfo(JSONUtil.toJsonStr(judgeInfo));
        update = questionSubmitService.updateById(questionSubmitUpdate);
        if (!update) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "题目状态更新错误");
        }
        //重新查询数据库并返回
        QuestionSubmit questionSubmitResult = questionSubmitService.getById(questionId);
        return questionSubmitResult;
    }
}
