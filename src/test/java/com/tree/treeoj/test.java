package com.tree.treeoj;

import java.util.Scanner;

/**
 * @author : Tree Arthur
 * @date: 2024/2/23 9:17
 * @Package: com.tree.treeoj
 * @Description: 数组输入
 * @version: 1.0
 */

public class test {
//    public static void main(String[] args) {
//        Scanner cin = new Scanner(System.in);
//        String str = cin.next().toString();
//        String[] arr = str.split(",");
//        int[] nums = new int[arr.length];
//
//        System.out.print("nums:");
//        for (int i = 0; i < nums.length; i++) {
//            nums[i] = Integer.parseInt(arr[i]);
//            System.out.print(nums[i] + " ");
//        }
//        System.out.println();
//
//        int target = cin.nextInt();
//        System.out.print("target:" + target);
//    }

    public int[] twoSum ( int[] nums, int target){
        int n = nums.length;
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[0];
    }

    public static void main (String[]args){
        Scanner cin = new Scanner(System.in);
        String str = cin.next().toString();
        String[] arr = str.split(",");
        int[] nums = new int[arr.length];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = Integer.parseInt(arr[i]);
        }

        int target = cin.nextInt();

        int[] res = new int[0];
        int n = nums.length;
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if (nums[i] + nums[j] == target) {
                    res = new int[]{i, j};
                }
            }
        }

        for (int i = 0; i < res.length; ++i) {
            System.out.print(res[i]);
            System.out.print(" ");
        }
    }
}
