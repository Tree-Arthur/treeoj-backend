package com.tree.treeoj;

import com.tree.treeoj.config.WxOpenConfig;

import javax.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Scanner;

/**
 * 主类测试
 *
 */
@SpringBootTest
class MainApplicationTests {

    @Resource
    private WxOpenConfig wxOpenConfig;

    @Test
    void contextLoads() {
        System.out.println(wxOpenConfig);
    }

    @Test
    void arrayScanner() {
        Scanner cin = new Scanner(System.in);
        String str = cin.next().toString();
        String[] arr = str.split(",");
        int[] nums = new int[arr.length];

        System.out.print("nums:");
        for (int i = 0; i < nums.length; i++) {
            nums[i] = Integer.parseInt(arr[i]);
            System.out.print(nums[i] + " ");
        }
        System.out.println();

        int target = cin.nextInt();
        System.out.print("target:"+target);
    }
}
